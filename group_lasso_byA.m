function [beta] = group_lasso_byA(task_train_x, task_train_y, lambda1, lambda2, p, k)

    miter = 1000;
    beta = zeros(p,k);
    oldobj = inf;
    th = 1e-4;
    
    mea = zeros(k,p); %mean of each tasks feature-vector
    va = ones(k,p)*-1;
    
    for i = 1:miter
        
        for j = 1:k
            X = task_train_x{j};
            Y = task_train_y{j};
            for k1=1:p
                if(~ all((X(1,k1)==X(:,k1))))
                    mea(j,k1) = mean(X(:,k1)) ;
                    va(j,k1) = var(X(:,k1),1) ;
                    X(:,k1) = ( X(:,k1)-mea(j,k1) ) / sqrt(va(j,k1)) ;
                end
            end
            
            n = length(Y) ;
            eta = 0.004;%1/eigs(X'*X/n,1); % = 1 / max-eigenValue of {X^T X}
    
            beta(:,j) = (1-eta*lambda2)*beta(:,j) + eta*X'*(Y-X*beta(:,j))/n; %Taking gradient descent update of ridge-regression objective for each task.
        end
        for j = 1:p
            beta(j,:) = beta(j,:)*max( 0, 1-(lambda1*eta)/norm(beta(j,:)) ); % [beta11,beta21,beta31,....,betak1] * max[0, 1-(lmabda1*eta/norm) ] <-This will make 0 to one feature of all tasks at a time. 
        end
        
        % Calculating complete objective (all tasks included).
        obj = 0;
        for j = 1:k
            X = task_train_x{j};
            Y = task_train_y{j};
            n = length(Y) ;
            obj = obj + (lambda2/2)*norm(beta(:,j))^2 + norm(Y - X*beta(:,j))^2/(2*n); % Sum the objective(ridge) of all tasks.
        end
        for j = 1:p
            obj = obj + lambda1*norm(beta(j,:)); % [So l1_norm -> Sum] the lambda1*norm of ith feature of all tasks. See below equation 3 in Nathan paper to see this.
        end
        
        %fprintf('Group lasso Obj of iteration %d : %f \n',i ,obj); %print the objective
        
        if (abs(oldobj-obj)<th)
           break; 
        end
        oldobj = obj;
    end
end