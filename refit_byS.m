function [beta] = refit_byS(X,Y,S)

s = length(S); % s = numer of non-zero features.
[n,p,k] = size(X);
beta = zeros(p,k);

for j = 1:k
    beta(S,j) = (reshape(X(:,S,j),n,s)'*reshape(X(:,S,j),n,s)) \ (reshape(X(:,S,j),n,s)'*Y(:,j)); % It's like (X'X) beta = X'Y => { beta = (X'X) \ X'Y } <= This "\" is used for estimate of least square.
    % Like for Xw=Y -> w=X\Y; and w=(X'*X)\(X'*Y) will give same answer i.e. of least square. 
   
    % So here it's like after group LASSO we get significant features. So now form X using sample at these features only i.e. X = nxs dimension
    % And now just use least square to get new beta estimates. This is refitted Group LASSO or Post Group LASSO.
end

end