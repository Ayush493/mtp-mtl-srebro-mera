function [beta] = group_lasso_byS(X,Y,lambda)

    miter = 1000;
    [n,p,k] = size(X); % n: #of samples per task, p: #of features and k: #of tasks
    eta = 1/eigs(reshape(X(:,:,1),n,p)'*reshape(X(:,:,1),n,p)/n,1); % = 1 / max-eigenValue of {X1^T X1}
    beta = zeros(p,k);
    oldobj = inf;
    th = 1e-4;

    for i = 1:miter
        for j = 1:k
            beta(:,j) = beta(:,j) + eta*reshape(X(:,:,j),n,p)'*(Y(:,j) - reshape(X(:,:,j),n,p)*beta(:,j))/n; %Taking gradient descent update of least-square objective for each task.
        end
        for j = 1:p
            beta(j,:) = beta(j,:)*max(0,1-(lambda*eta)/norm(beta(j,:),2)); % [beta11,beta21,beta31,....,betak1] * max[0, 1-(lmabda*eta/norm) ] <-This will make 0 to one feature of all tasks at a time. 
        end
        
        % Calculating complete objective (all tasks included).
        obj = 0;
        for j = 1:k
            obj = obj + norm(Y(:,j) - reshape(X(:,:,j),n,p)*beta(:,j))^2/(2*n); % Sum the objective(least-square) of all tasks.
        end
        for j = 1:p
            obj = obj + lambda*norm(beta(j,:)); % [So l1_norm -> Sum] the lambda*norm of ith feature of all tasks. See below equation 3 in Nathan paper to see this.
        end
        fprintf('Group lasso Obj: %f \n',obj);
        
        if (abs(oldobj-obj)<th)
           break; 
        end
        oldobj = obj;
    end
end