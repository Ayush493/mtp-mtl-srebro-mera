function [beta] = lasso_byS(X,Y,lambda)

miter = 1000;
[n,p] = size(X);
eta = 0.004 ; %1/eigs(X'*X/n,1);
beta = zeros(p,1);
oldobj = inf;
th = 1e-4;

for i = 1:miter
    beta = beta + eta*X'*(Y - X*beta)/n; %Simple gradient descent update
    beta = wthresh_byS(beta,'s',eta*lambda); %Soft-thresholding or projection of gradient-descent update.
    
    obj = norm(Y - X*beta)^2/(2*n) + lambda*norm(beta,1);
    %fprintf('Lasso Obj: %f \n',obj);  %Commented by Ayush.
    if (abs(oldobj-obj)<th)
       break; 
    end
    oldobj = obj;
end

end

