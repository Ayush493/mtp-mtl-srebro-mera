function [out] = expVarNRmse(X,Y,X_test,Y_test,w)
    
    out = zeros(1:5) ;
    
    % Number of non-zero elements in weight vector.
    out(1) = sum(w~=0) ;

    %Calculating explained variance in train-set using same formula of R-square :
    SStot = sum( (Y-mean(Y)).^2 ) ; 
    SSres = sum( (Y-X*w).^2 ) ;
    out(2) = ( 1-(SSres/SStot) ) * 100 ;%This formula is of R-square see its wiki page

    %Calculating explained variance in test-set using same formula of R-square :        
    SStot_test = sum( (Y_test-mean(Y_test)).^2 ) ; 
    SSres_test = sum( (Y_test-X_test*w).^2 ) ;
    out(3) = ( 1-(SSres_test/SStot_test) ) * 100 ;%This formula is of R-square see its wiki page

    %nMSE and RMSE calculation
    MSE = SSres_test/length(Y_test) ; %over test set
    variance = SStot_test/length(Y_test-1) ; %over test set: variance of test-response variable and unbiased estimate that's why length-1 in denominator. 
    out(4) = MSE/variance ; %nMSE
    
    out(5) = sqrt(MSE); %over test set: RMSE
    
end
