function [M] = estimateM_byS(sigmahat,nu)

    p = size(sigmahat,1);
    M = zeros(p,p);

    for i = 1:p
        rowp = rowestimate(nu,i,sigmahat);
        M(i,:) = rowp;
    end

    M = -M;
end

function rowp = rowestimate(nu,i,sigmahat)
    p = size(sigmahat,1);
    rowp = zeros(1,p);
    
    for iter = 1:100 
       oldrowp = rowp;
       for j = 1:p % For each coordinate.
           temp = rowp;
           temp(j) = 0;
           rowp(j) = wthresh_byS(-sigmahat(j,:)*temp'-(i==j),'s',nu)/sigmahat(j,j); %Note this 1st term is soft-thresholding like for " l1-constraint <= t ". Also the first term is exactly what is described in paper below algorithm-1
           % For 'ej' vector it's one at jth and rest 0, same is here as we are passing 'i' in argument and we need 1 at that and 0 at rest so while processing coordinate wise we kept i==j.
           % Here 'rowp' is playing role of 'mj' from paper. Don't know why but for the processing coordinate we make its value 0 in temp and also about divide by sigmahat(j,j).
       end
       if (norm(oldrowp-rowp,2)<0.5*1e-2)
%            fprintf('Takes %d ierations to converge.\n',iter);
           break; 
       end
       
    end
end

