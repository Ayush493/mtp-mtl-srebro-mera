% In this file wherever I refer to paper it's AISTAT verison of Srebro's work.
function [] = Main_simulation_byS(n,p,k,s) % or (n,p,m,s) 
% n= number of samples per task
% p= number of features per task
% m or k= number of tasks
% s= I need to see this

sigma = eye(p); %Identity matrix of size "pxp".
for i = 1:p
    for j = 1:p
        sigma(i,j) = 2^(-abs(i-j)); %Creating the covariance matrix for simulated data-set
    end
end
M = inv(sigma);

% niter = 200;
niter = 10; % For these many times we will create data and will apply various methods.

h_lasso = zeros(niter,1); % h stands for hamming distance: We can have this concept only here i.e. we are generating data-set so we actually know exact number of non-zero features and so we can calculate hamming distance with the predictors. Note such hamming distance concept is not there in SCHOOL data-set.
h_group = zeros(niter,1);
h_dsml = zeros(niter,1);
%h_icap = zeros(niter,1);

e_lasso = zeros(niter,1); % e stands for estimation error 
e_group = zeros(niter,1);
e_dsml = zeros(niter,1);
%e_icap = zeros(niter,1);
e_group_refit = zeros(niter,1);

p_lasso = zeros(niter,1); % p stands for prediction error.
p_group = zeros(niter,1);
p_dsml = zeros(niter,1);
%p_icap = zeros(niter,1);
p_group_refit = zeros(niter,1);

for iter = 1:niter
    beta = zeros(p,k); % k columns => k tasks and each column will have estimated weight(beta) vector for that task. So "p" rows.
    beta(1:s,:) = rand(s,k); % Keeping 1st "s" features of all tasks as non-zero and rest all features as zero. 
    
    X = zeros(n,p,k); % 3D storage. "k" tasks so for each task we will have "nxp" dimensions of data-"X".
    Y = zeros(n,k);
    X_test = zeros(n,p,k); % NOTE: here the test-set are also of same dimension or quantity. Like n samples in train and test set.
    Y_test = zeros(n,k);
    
    for j = 1:k % For each task creating X and Y.
        X(:,:,j) = mvnrnd(zeros(1,p),sigma,n); %mvrnd returns nxp matrix consists of n-random vectors each of size 1xp chosen from the multivariate normal distribution with a common 1xp mean vector, and a common pxp covariance matrix "sigma".
        Y(:,j) = reshape(X(:,:,j),n,p)*beta(:,j) + randn(n,1); 
    end
    
    for j = 1:k % For each task again creating X and Y for testing purpose
        X_test(:,:,j) = mvnrnd(zeros(1,p),sigma,n); 
        Y_test(:,j) = reshape(X_test(:,:,j),n,p)*beta(:,j)+ randn(n,1); 
    end
    
    % ------------------------------------------ Simple Local Lasso
    beta_lasso = zeros(p,k);
    beta_lasso_predict = zeros(p,k);
    for j = 1:k % For each task
        best_dist = inf;
        best_pred = inf;
        for lambda = 2.^[-5:5]*sqrt(log(p)/n) %Trying various values of lambda. And for each lambda we are keeping track of 2 best lasso estimates 1st from aspect of min hamming error and 2nd from aspect of closeness to original predictor
            hat_betaj = lasso_byS(reshape(X(:,:,j),n,p),Y(:,j),lambda);
            if (p - sum((beta(:,j)==0)==(hat_betaj==0)) < best_dist)
                best_dist = p - sum((beta(:,j)==0)==(hat_betaj==0));
                beta_lasso(:,j) = hat_betaj;
            end
            if norm(hat_betaj-beta(:,j)) < best_pred %NOTE: here take 1st difference of est_weight vector and original_wt vector used to generate data and then get its l2-norm.
                best_pred = norm(hat_betaj-beta(:,j));
                beta_lasso_predict(:,j) = hat_betaj;
            end %NOTE: for different values of lambda we might get -> best_lasso_(:,1) and best_lasso_predict(:,1) say for 1st task.
        end
    end
    
    % --------------------------------Group Lasso by Srebro's definition below eq3 in paper.
    beta_group = zeros(p,k);
    beta_group_predict = zeros(p,k);    
    best_dist = inf;
    best_pred = inf;
    
    for lambda = 2.^[-5:5]*sqrt((k*log(p))/n) % NOTE the choice of 'lambda' in terms of sqrt[k*log(p)/n] and 2.^[-1:1] => 0.5, 1, 2
        hat_beta = group_lasso_byS(X,Y,lambda);
        
        if (p*k - sum(sum((beta==0)==(hat_beta==0))) < best_dist)
           best_dist =  p*k - sum(sum((beta==0)==(hat_beta==0)));
           beta_group = hat_beta;
           S = find(beta_group(:,1)'); % ind = find(X) locates all nonzero elements of array X, and returns the linear indices of those elements in vector "ind". This will help in "refitted group lasso" method.
        end
        if (norm(hat_beta - beta,'fro') < best_pred) % Frobenius norm of 'residual matrix' i.e. get residual matrix then take sqrt of sum of square of each element.
           best_pred = norm(hat_beta - beta,'fro');
           beta_group_predict = hat_beta;
        end %NOTE: for different values of lambda we might get -> best_group and best_group_predict.
    end
    
    % --------------------------------Refitted Group Lasso or Post group lasso. Only for toy data-set.
    beta_group_refit = refit_byS(X,Y,S); 

    %{ 
        %multi-line comment------------------------For now iCAP is of no use. 
        best_dist = inf;
        beta_icap = zeros(p,k);

        for lambda = 2.^[-5:5]*sqrt((k*log(p))/n)
            hat_beta = icap_lasso(X,Y,lambda);
            if (p*k - sum(sum((beta==0)==(hat_beta==0))) < best_dist)
               best_dist =  p*k - sum(sum((beta==0)==(hat_beta==0)));
               beta_icap = hat_beta;
               S = find(beta_icap(:,1)');
            end
        end
    %}

    % --------------------------------DSML = Distributed debiased Sparse Multi-task Lasso = Their algo.
    beta_lasso_debiased = zeros(p,k);
    beta_group_debiased = zeros(p,k);
    nu = sqrt(log(p)/n); % This is used in debiasing as mentioned on page-3 of Srebro's paper. And this formula is written on Page-6 top-right para. 
    
    % Stage-1 Debias locally
    for j = 1:k % For each task
        sigmahat = (reshape(X(:,:,j),n,p)'*reshape(X(:,:,j),n,p))/n; % The nomenclature is as mentioned on pg-3 below algorithm-1.
        M = estimateM_byS(sigmahat,nu);
        beta_lasso_debiased(:,j) = beta_lasso_predict(:,j) + (1/n)*M*reshape(X(:,:,j),n,p)'*(Y(:,j)-reshape(X(:,:,j),n,p)*beta_lasso_predict(:,j)); % pg-3 : eq-4 : Debiasing formula = Newton update step
        beta_group_debiased(:,j) = beta_group_predict(:,j) + (1/n)*M*reshape(X(:,:,j),n,p)'*(Y(:,j)-reshape(X(:,:,j),n,p)*beta_group_predict(:,j));
    end

    % Stage-2 Find a suitable lambda for hard-thresholding.
    beta_GHT = beta_lasso_debiased;
    best_dist = inf;
    for lambda = 2.^[-5:5]*sqrt((log(p)*k)/n)
        for j = 1:p
            hat_beta(j,:) = beta_lasso_debiased(j,:)*(norm(beta_lasso_debiased(j,:)) > lambda);
        end
        if (p*k - sum(sum((beta==0)==(hat_beta==0))) < best_dist) % Again selecting based on best Hamming distance.
           best_dist =  p*k - sum(sum((beta==0)==(hat_beta==0)));
           beta_GHT = hat_beta;
        end    
    end
    %------------- DSML over.
    
    fprintf('Best Hamming Distance for lasso: %d\n',p*k - sum(sum((beta==0)==(beta_lasso==0))));
    fprintf('Best Hamming Distance for group_lasso: %d\n',p*k - sum(sum((beta==0)==(beta_group==0))));
    %fprintf('Best Hamming Distance for icap: %d\n',p*k - sum(sum((beta==0)==(beta_icap==0))));
    fprintf('Best Hamming Distance for distributed GHT: %d\n',p*k - sum(sum((beta==0)==(beta_GHT==0))));

    h_lasso(iter) = p - sum(sum((beta==0)==(beta_lasso==0)))/k;
    h_group(iter) = p - sum(sum((beta==0)==(beta_group==0)))/k;
    % h_icap(iter) = p - sum(sum((beta==0)==(beta_icap==0)))/k;
    h_dsml(iter) = p - sum(sum((beta==0)==(beta_GHT==0)))/k;
    
    e_lasso(iter) = norm(beta-beta_lasso,'fro')^2/k; % estimation error
    e_group(iter) = norm(beta-beta_group,'fro')^2/k;
    % e_icap(iter) = norm(beta-beta_icap,'fro')^2/k;
    e_dsml(iter) = norm(beta-beta_GHT,'fro')^2/k;
    e_group_refit(iter) = norm(beta-beta_group_refit,'fro')^2/k;

    for j = 1:k % For each task: calculating prediction error on Test-set
        p_lasso(iter) = p_lasso(iter) + (1/k)*norm(Y_test(:,j) - reshape(X_test(:,:,j),n,p)*beta_lasso(:,j))/sqrt(n); % Y-Y^ = norm(Y-Xw)/sqrt(n) And average these out for all tasks, so 1/k.
        p_group(iter) = p_group(iter) + (1/k)*norm(Y_test(:,j) - reshape(X_test(:,:,j),n,p)*beta_group(:,j))/sqrt(n);
        %p_icap(iter) = p_icap(iter) + (1/k)*norm(Y_test(:,j) - reshape(X_test(:,:,j),n,p)*beta_icap(:,j))/sqrt(n);
        p_dsml(iter) = p_dsml(iter) + (1/k)*norm(Y_test(:,j) - reshape(X_test(:,:,j),n,p)*beta_GHT(:,j))/sqrt(n);
        p_group_refit(iter) = p_group_refit(iter) + (1/k)*norm(Y_test(:,j) - reshape(X_test(:,:,j),n,p)*beta_group_refit(:,j))/sqrt(n);
    end

end

%save(strcat('simulation_n_',num2str(n),'_p_',num2str(p),'_k_',num2str(k),'_s_',num2str(s),'.mat'),'h_lasso','h_group','h_dsml','h_icap','e_lasso','e_group','e_dsml','e_icap','p_lasso','p_group','p_dsml','p_icap','e_group_refit','p_group_refit');
% removing iCAP variables from saving in .mat format.
save(strcat('simulation_n_',num2str(n),'_p_',num2str(p),'_k_',num2str(k),'_s_',num2str(s),'.mat'),'h_lasso','h_group','h_dsml','e_lasso','e_group','e_dsml','p_lasso','p_group','p_dsml','e_group_refit','p_group_refit');

%fprintf('Hamming distance: Lasso: &%.1f \t$\\pm$ %.1f \t& Group: %.1f \t$\\pm$ %.1f \t& ICAP: %.1f \t$\\pm$ %.1f \t& DSML: %.1f \t$\\pm$ %.1f \t \\\\\n',mean(h_lasso),std(h_lasso),mean(h_group),std(h_group),mean(h_icap),std(h_icap),mean(h_dsml),std(h_dsml));
fprintf('Hamming distance: Lasso: &%.1f \t$\\pm$ %.1f \t& Group: %.1f \t$\\pm$ %.1f \t& DSML: %.1f \t$\\pm$ %.1f \t \\\\\n',mean(h_lasso),std(h_lasso),mean(h_group),std(h_group),mean(h_dsml),std(h_dsml));
fprintf('Estimation error: Lasso: &%.1f \t$\\pm$ %.1f \t& Group: %.1f \t$\\pm$ %.1f \t& DSML: %.1f \t$\\pm$ %.1f \t \\\\\n',mean(e_lasso),std(e_lasso),mean(e_group),std(e_group),mean(e_dsml),std(e_dsml));
fprintf('Prediction error: Lasso: &%.1f \t$\\pm$ %.1f \t& Group: %.1f \t$\\pm$ %.1f \t& DSML: %.1f \t$\\pm$ %.1f \t \\\\\n',mean(p_lasso),std(p_lasso),mean(p_group),std(p_group),mean(p_dsml),std(p_dsml));

end
