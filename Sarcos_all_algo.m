
clearvars ; %just for clearing the global environment
lambda1 = 0.5; % lambda1 is for l1-norm and lambda2 is for l2-norm regularization. 
lambda2 = 0.1;
lambda = 4.5 ; % Only for hard-thresholding in DSML of Srebro.
s=10; % number of non-zero features required.

T=7; %Number of tasks, max we can give is 7.
nf=22; %21 features + 1 bias term
num_experiments = 1;

%Out of the following 3 whichever algo u want to run just press-1.
local_l1_norm = 1;
group_l1_norm = 0;
dsml = 0;

load('/media/laxman/Data/Ayush/data/mtl_sarcos_data/sarcos_inv.mat')
raw_train_data = sarcos_inv;

load('/media/laxman/Data/Ayush/data/mtl_sarcos_data/sarcos_inv_test.mat')
raw_test_data = sarcos_inv_test;

for m = 1:num_experiments
    
    count_tr = zeros(1,T) ; %store #of training samples per task.
    count_tst = zeros(1,T) ; %store #of test samples per task.
    tNum = ceil(sqrt(T)) ;
    task_train_x = cell(tNum); %there are 7 tasks and this cell will contain 3x3=9 empty matrices which can be accessed in sequence. i.e. ceil(sqrt(7))
    task_train_y = cell(tNum);
    task_test_x = cell(tNum);
    task_test_y = cell(tNum);
    
    a = '/media/laxman/Data/Ayush/data/mtl_sarcos_data/by_me/sarcos_';
    
    %reading training indices
    b = '_tr_indexes.txt';
    c = strcat(a, int2str(m), b) ; 
    fileID = fopen(c,'r');
    [train_index_arr,tr_samples] = fscanf(fileID,'%d'); %this will read "array indices" from file in "int" format and will store as column vector. It will also store the count of items it reads in 2nd argument. 
    
    %reading testing indices
    b = '_tst_indexes.txt';
    c = strcat(a, int2str(m), b) ; 
    fileID = fopen(c,'r');
    [test_index_arr,tst_samples] = fscanf(fileID,'%d');
    
    %Train data creation
    %temporary storages for training data creation
    temp2dDummyBuffer = zeros(tr_samples,nf); %store the train X.
    temp2dArray_tr = zeros(tr_samples,T); %temporarily it will store Y of 7 tasks
    for i= 1:tr_samples
        index = train_index_arr(i,1)+1; %train_index_arr is a column vector and +1 bcoz the indices in file can also contain "0"th index. "If you get time then convert in the basic file to index+1."
        
        temp2dDummyBuffer(i,1:nf-1) = raw_train_data(index,1:nf-1) ; %no need of for loop, here directly values from rhs will be copied to specified indexes in lhs.
        temp2dDummyBuffer(i,nf) = 1;  %bias term
 
        temp2dArray_tr(i,1:T) = raw_train_data(index,nf:nf+T-1) ; %storing Y vector of 7 tasks here. 
    end    
    for i=1:T
        count_tr(i) = tr_samples;
        task_train_x{i} = temp2dDummyBuffer ; %for each task we have now training X
        task_train_y{i} = temp2dArray_tr(:,i) ; %for each task we have now training Y (column vector)
    end
    
 
    %Test data creation : exactly same like train data-generation above
    %temporary storages for test data creation
    temp2dDummyBuffer_tst = zeros(tst_samples,nf); %store the test X.
    temp2dArray_tst = zeros(tst_samples,T); %temporarily it will store Y of 7 tasks
    for i= 1:tst_samples
        index = test_index_arr(i,1)+1; %test_index_arr is a column vector and +1 bcoz the indices in file can also contain "0"th index.
        temp2dDummyBuffer_tst(i,1:nf-1) = raw_test_data(index,1:nf-1) ; %no need of for loop, here directly values from rhs will be copied to specified indexes in lhs.
        temp2dDummyBuffer_tst(i,nf) = 1;  %bias term
        temp2dArray_tst(i,1:T) = raw_test_data(index,nf:nf+T-1) ; %storing Y vector of 7 tasks here. 
    end
    for i=1:T
        count_tst(i) = tst_samples;
        task_test_x{i} = temp2dDummyBuffer_tst ; %for each task we have now testing X
        task_test_y{i} = temp2dArray_tst(:,i) ; %for each task we have now testing Y (column vector)
    end
    %Data creation completed.
    
    
    %Creating some output measures
    exp_var_train = zeros(1,T);
    exp_var_test = zeros(1,T);
    nMSE = zeros(1,T);
    RMSE = zeros(1,T);
    num_nonzero = zeros(1,T); % this will store number of non-zero coordinates in weight vector of each task.
    
    % 2 vectors for normalization of data.
    mea = zeros(T,nf); %mean of each tasks feature-vector
    va = ones(T,nf)*-1; %variance of each tasks feature-vector: storing -1 at each place.

    if local_l1_norm == 1
        %running LASSO for each task
        for t=1:T
            X = task_train_x{t};
            Y = task_train_y{t};
            X_test = task_test_x{t} ;
            Y_test = task_test_y{t} ;
            %best_nMSE = inf; % We can't choose best lambda1 from "for" loop, as on what basis we will choose? Like min nMSE but note we won't get sparsity, as more non-zero features => min nMSE. So in this case find using
            % particular value of lambda.

            %for lambda1 = 2.^[-5:5]*sqrt(log(nf)/count_tr(t))
            
            %Normalization of train data: Getting z-scores X = zscore(X,1); %I can't use this as for same value columns, it will make them 0. And this seems fair enough as finding weights for such features is meaningless. 
            % I think I should use zscore on X and then add the last column with all 1's for bias term. But I can't do that too bcoz for test-set I should normalize them using already known mean and variance of features, 
            % bcoz think of each sample of test-set as unknown, like one-by-one they are %coming and we need to normalize them. 
            for i=1:nf
                if(~ all((X(1,i)==X(:,i))))
                    mea(t,i) = mean(X(:,i)) ;
                    va(t,i) = var(X(:,i),1) ;
                    X(:,i) = ( X(:,i)-mea(t,i) ) / sqrt(va(t,i)) ;
                end
            end
            %Normalizing of test data-set:
            for i=1:nf
                if(va(t,i)>0)
                    X_test(:,i) = ( X_test(:,i)-mea(t,i) ) / sqrt(va(t,i)) ;
                end
            end
        
            w =lasso_byA(X,Y,lambda1,lambda2) ;

            %Calculating all the output measures
            output = expVarNRmse(X,Y,X_test,Y_test,w) ; 
            
            num_nonzero(t) = output(1) ;
            exp_var_train(t) = output(2) ;
            exp_var_test(t) = output(3) ;
            nMSE(t) = output(4) ;
            RMSE(t) = output(5) ;
        end
        
    elseif group_l1_norm == 1 %In this case a small change in lambda-2 gives pretty much difference in exp_variances tested over few ranges.
        %running Group l1 norm for multi-tasking. Here communication happens multiple times.
        W = group_lasso_byA(task_train_x, task_train_y, lambda1, lambda2, nf, T); % W would be nfxT dimensions i.e. jth column will correspond to jth task weight vector.
        
        for t=1:T
            X = task_train_x{t};
            Y = task_train_y{t};
            X_test = task_test_x{t} ;
            Y_test = task_test_y{t} ;
            
            w = W(:,t) ; 
            
            %Normalization for output measures:
            for i=1:nf
                if(~ all((X(1,i)==X(:,i))))
                    mea(t,i) = mean(X(:,i)) ;
                    va(t,i) = var(X(:,i),1) ;
                    X(:,i) = ( X(:,i)-mea(t,i) ) / sqrt(va(t,i)) ;
                end
            end
            %Normalizing of test data-set:
            for i=1:nf
                if(va(t,i)>0)
                    X_test(:,i) = ( X_test(:,i)-mea(t,i) ) / sqrt(va(t,i)) ;
                end
            end
            
            %Calculating all the output measures
            output = expVarNRmse(X,Y,X_test,Y_test,w) ; 
            
            num_nonzero(t) = output(1) ;
            exp_var_train(t) = output(2) ;
            exp_var_test(t) = output(3) ;
            nMSE(t) = output(4) ;
            RMSE(t) = output(5) ;
        end
        
    elseif dsml == 1 %As of now "dsml" focuses on debiasing of lasso but our objective is different (l2+l0). So if we can find a way for dsml to work (l2+l1), then I may use local_l1_norm updates and debias them but now estimating lasso updates locally and then debiasing.
        W = dsml_byA(task_train_x, task_train_y, lambda1, lambda, nf, T, s);
        
        for t=1:T
            X = task_train_x{t};
            Y = task_train_y{t};
            X_test = task_test_x{t} ;
            Y_test = task_test_y{t} ;
            
            w = W(:,t) ; 
            
            %Normalization for output measures:
            for i=1:nf
                if(~ all((X(1,i)==X(:,i))))
                    mea(t,i) = mean(X(:,i)) ;
                    va(t,i) = var(X(:,i),1) ;
                    X(:,i) = ( X(:,i)-mea(t,i) ) / sqrt(va(t,i)) ;
                end
            end
            %Normalizing of test data-set:
            for i=1:nf
                if(va(t,i)>0)
                    X_test(:,i) = ( X_test(:,i)-mea(t,i) ) / sqrt(va(t,i)) ;
                end
            end
            
            %Calculating all the output measures
            output = expVarNRmse(X,Y,X_test,Y_test,w) ; 
            
            num_nonzero(t) = output(1) ;
            exp_var_train(t) = output(2) ;
            exp_var_test(t) = output(3) ;
            nMSE(t) = output(4) ;
            RMSE(t) = output(5) ;
        end
    end
    
    AvgVarTrain = mean(exp_var_train);
    AvgVarTest = mean(exp_var_test);
    AvgnMse = mean(nMSE);
    AvgRmse = mean(RMSE);
    AvgNonZero = mean(num_nonzero);
   % AvgObjective = mean(objective_val) ;
    disp(['Non-zero_terms:' num2str(AvgNonZero) ' Train_var_exp:' num2str(AvgVarTrain) ' Test_var_exp:' num2str(AvgVarTest) ' nMSE:' num2str(AvgnMse) ' Rmse:' num2str(AvgRmse) ]) 
end