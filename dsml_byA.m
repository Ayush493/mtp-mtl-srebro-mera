function [temp_beta] = dsml_byA(task_train_x, task_train_y, lambda1, lambda, p, k, s)
    
    beta = zeros(p,k) ;
    temp_beta = zeros(p,k) ;
    temp_beta1 = zeros(p,k) ;
    num = zeros(1,1) ;
    mea = zeros(k,p); %mean of each tasks feature-vector
    va = ones(k,p)*-1;
    
    for j= 1:k
        X = task_train_x{j};
        Y = task_train_y{j};
        
        for k=1:p
            if(~ all((X(1,k)==X(:,k))))
                mea(j,k) = mean(X(:,k)) ;
                va(j,k) = var(X(:,k),1) ;
                X(:,k) = ( X(:,k)-mea(j,k) ) / sqrt(va(j,k)) ;
            end
        end
            
        n = length(Y) ;
        num = n;
        
        beta(:,j) = lasso_byS(X,Y,lambda1);
       
        %{ 
        Printing the weight vector per task.
        g=sprintf('%f ', beta(:,j));
        fprintf('Answer: %s\n', g) ;
       %}
       
        % Debiasing step
        nu = sqrt(log(p)/n); % This is used in debiasing as mentioned on page-3 of Srebro's paper. And this formula is written on Page-6 top-right para. 
        sigmahat = (X'*X)/n ; % The nomenclature is as mentioned on pg-3 below algorithm-1.
        M = estimateM_byS(sigmahat,nu);
        beta(:,j) = beta(:,j) + (1/n)*M*X'*(Y-X*beta(:,j)); % pg-3 : eq-4 : Debiasing formula = Newton update step
        
        %{
        g=sprintf('%f ', beta(:,j));
        fprintf('Answer: %s\n', g) ;
        %}
        
    end
    
    %hard threholding at central/master node only once.
    for j = 1:p
        temp_beta(j,:) = beta(j,:)*(norm(beta(j,:)) > lambda);
    end
    %{
    init = p ;
    for lam = 2.^[-5:5] *sqrt((log(p)*k)/num)
        for j = 1:p
            temp_beta1(j,:) = beta(j,:)*(norm(beta(j,:)) > lam);
        end
        w = temp_beta1(:,1) ; 
        if sum(w~=0)<=init
            init = sum(w~=0) ;
            temp_beta = temp_beta1 ;
            break ;
        end
    end
    %}
end