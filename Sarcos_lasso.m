%NOTE no need to look for objective value as MTL-single task will have ridge regression objective while here we will have lasso objective. So this is not a proper comparison. Also earlier I ran this LASSO at 1st task
%for a particular value of lambda and find #of non-zero weights "s". Then I submit this "s" as input to MTL-SIngle task code of scala where Pratik's algo i.e. IHT(Iterative Hard Thresholding) is implemented.
% So for proper comparison, I will compare L0 norm over ridge-regression obj to L1 norm over ridge-regression obj, this is simply verification of Pratik's work as in his paper he showed L0 over Least-square is better
% than L1 over Least-square i.e. Lasso OR. Hard threholding is better than Soft thresholding.

clearvars ; %just for clearing the global environment
lambda = 0.1; %regularization term NOTE: more its value more sparsity i.e. more coordinates will be made zero.
num_experiments = 1;  %As of now don't keep other values bcoz we got Number of nonzero terms vector and it can't be averaged over number of experiments.

T=1; %Number of tasks
nf=22; %21 features + 1 bias term

load('/media/laxman/Data/Ayush/data/mtl_sarcos_data/sarcos_inv.mat')
raw_train_data = sarcos_inv;

load('/media/laxman/Data/Ayush/data/mtl_sarcos_data/sarcos_inv_test.mat')
raw_test_data = sarcos_inv_test;

for m = 1:num_experiments
    
    count_tr = zeros(1,T) ; %store #of training samples per task.
    count_tst = zeros(1,T) ; %store #of test samples per task.
    tNum = ceil(sqrt(T)) ;
    task_train_x = cell(tNum); %there are 7 tasks and this cell will contain 3x3=9 empty matrices which can be accessed in sequence. i.e. ceil(sqrt(7))
    task_train_y = cell(tNum);
    task_test_x = cell(tNum);
    task_test_y = cell(tNum);
    
    a = '/media/laxman/Data/Ayush/data/mtl_sarcos_data/by_me/sarcos_';
    
    %reading training indices
    b = '_tr_indexes.txt';
    c = strcat(a, int2str(m), b) ; 
    fileID = fopen(c,'r');
    [train_index_arr,tr_samples] = fscanf(fileID,'%d'); %this will read "array indices" from file in "int" format and will store as column vector. It will also store the count of items it reads in 2nd argument. 
    
    %reading testing indices
    b = '_tst_indexes.txt';
    c = strcat(a, int2str(m), b) ; 
    fileID = fopen(c,'r');
    [test_index_arr,tst_samples] = fscanf(fileID,'%d');
    
    %Train data creation
    %temporary storages for training data creation
    temp2dDummyBuffer = zeros(tr_samples,nf); %store the train X.
    temp2dArray_tr = zeros(tr_samples,T); %temporarily it will store Y of 7 tasks
    for i= 1:tr_samples
        index = train_index_arr(i,1)+1; %train_index_arr is a column vector and +1 bcoz the indices in file can also contain "0"th index.
        
        temp2dDummyBuffer(i,1:nf-1) = raw_train_data(index,1:nf-1) ; %no need of for loop, here directly values from rhs will be copied to specified indexes in lhs.
        temp2dDummyBuffer(i,nf) = 1;  %bias term
 
        temp2dArray_tr(i,1:T) = raw_train_data(index,nf:nf+T-1) ; %storing Y vector of 7 tasks here. 
    end
    
    for i=1:T
        count_tr(i) = tr_samples;
        task_train_x{i} = temp2dDummyBuffer ; %for each task we have now training X
        task_train_y{i} = temp2dArray_tr(:,i) ; %for each task we have now training Y (column vector)
    end
    
 
    %Test data creation : exactly same like train data-generation above
    %temporary storages for test data creation
    temp2dDummyBuffer_tst = zeros(tst_samples,nf); %store the test X.
    temp2dArray_tst = zeros(tst_samples,T); %temporarily it will store Y of 7 tasks
    for i= 1:tst_samples
        index = test_index_arr(i,1)+1; %test_index_arr is a column vector and +1 bcoz the indices in file can also contain "0"th index.
        temp2dDummyBuffer_tst(i,1:nf-1) = raw_test_data(index,1:nf-1) ; %no need of for loop, here directly values from rhs will be copied to specified indexes in lhs.
        temp2dDummyBuffer_tst(i,nf) = 1;  %bias term
        temp2dArray_tst(i,1:T) = raw_test_data(index,nf:nf+T-1) ; %storing Y vector of 7 tasks here. 
    end
    for i=1:T
        count_tst(i) = tst_samples;
        task_test_x{i} = temp2dDummyBuffer_tst ; %for each task we have now testing X
        task_test_y{i} = temp2dArray_tst(:,i) ; %for each task we have now testing Y (column vector)
    end
    %Data creation completed.
    
    
    %Creating some output measures
    exp_var_train = zeros(1,T);
    exp_var_test = zeros(1,T);
    nMSE = zeros(1,T);
    RMSE = zeros(1,T);
    num_nonzero = zeros(1,T); % this will store number of non-zero coordinates in weight vector of each task.
    %objective_val = zeros(1,T);
    mea = zeros(T,nf); %mean of each tasks feature-vector
    va = ones(T,nf)*-1; %variance of each tasks feature-vector: storing -1 at each place.
    
    %running LASSO for each task
    for t=1:T
        X = task_train_x{t};
        %Normalization of train data: Getting z-scores X = zscore(X,1); %I can't use this as for same value columns, it will make them 0. And this seems fair enough as finding weights for such features is meaningless. 
            % I think I should use zscore on X and then add the last column with all 1's for bias term. But I can't do that too bcoz for test-set I should normalize them using already known mean and variance of features, 
            % bcoz think of each sample of test-set as unknown, like one-by-one they are %coming and we need to normalize them. 
        for i=1:nf
            if(~ all((X(1,i)==X(:,i))))
                mea(t,i) = mean(X(:,i)) ;
                va(t,i) = var(X(:,i),1) ;
                X(:,i) = ( X(:,i)-mea(t,i) ) / sqrt(va(t,i)) ;
            end
        end
        
        Y = task_train_y{t};
        
        cvx_begin quiet
            cvx_precision low
            variable w(nf) %w is column vector
            minimize(0.5*sum_square(Y-X*w)/count_tr(t) + lambda*norm(w,1))
        cvx_end
      
        % objective_val(t) = cvx_optval ;
        % w_srebro =lasso_byS(X,Y,lambda) ; %gives same result as CVX.
        
        w(abs(w)<1e-03) = 0;
        % Number of non-zero elements in weight vector.
        num_nonzero(t) = sum(w~=0) ;
        
        %Calculating explained variance in train-set using same formula of R-square :
        SStot = sum( (Y-mean(Y)).^2 ) ; 
        SSres = sum( (Y-X*w).^2 ) ;
        exp_var_train(t) = ( 1-(SSres/SStot) ) * 100 ;%This formula is of R-square see its wiki page
        
        %Calculating explained variance in test-set using same formula of R-square :        
        X_test = task_test_x{t} ;
        Y_test = task_test_y{t} ;
        %Normalizing of test data-set:
        for i=1:nf
            if(va(t,i)>0)
                X_test(:,i) = ( X_test(:,i)-mea(t,i) ) / sqrt(va(t,i)) ;
            end
        end
        
        SStot_test = sum( (Y_test-mean(Y_test)).^2 ) ; 
        SSres_test = sum( (Y_test-X_test*w).^2 ) ;
        exp_var_test(t) = ( 1-(SSres_test/SStot_test) ) * 100 ;%This formula is of R-square see its wiki page
        
        %nMSE and RMSE calculation
        MSE = SSres_test/length(Y_test) ; %over test set
        RMSE(t) = sqrt(MSE); %over test set
        variance = SStot_test/length(Y_test-1) ; %over test set: variance of test-response variable and unbiased estimate that's why length-1 in denominator. 
        nMSE(t) = MSE/variance ;
        
    end
    
    AvgVarTrain = mean(exp_var_train);
    AvgVarTest = mean(exp_var_test);
    AvgnMse = mean(nMSE);
    AvgRmse = mean(RMSE);
    AvgNonZero = mean(num_nonzero);
   % AvgObjective = mean(objective_val) ;
    disp(['Non-zero_terms:' num2str(AvgNonZero) ' Train_var_exp:' num2str(AvgVarTrain) ' Test_var_exp:' num2str(AvgVarTest) ' nMSE:' num2str(AvgnMse) ' Rmse:' num2str(AvgRmse) ]) 
end

