function [beta] = lasso_byA(X,Y,lambda1,lambda2)
% lambda2 is for l2-norm regularization while lambda1 is for l1-norm.

miter = 1000;
[n,p] = size(X);
eta = 0.004; %1/eigs(X'*X/n,1);
beta = zeros(p,1);
oldobj = inf;
th = 1e-4;

for i = 1:miter
    beta = (1-eta*lambda2)*beta + eta*X'*(Y - X*beta)/n; %Simple gradient descent update: Here the update is for ridge-regression objective.
    beta = wthresh_byS(beta,'s',eta*lambda1); %Soft-thresholding or projection of gradient-descent update.
    
    obj = (lambda2/2)*norm(beta)^2 + norm(Y - X*beta)^2/(2*n) + lambda1*norm(beta,1) ;
    %fprintf('Lasso Obj at iter %d : %f \n',i,obj);  %Commented by Ayush.
    if (abs(oldobj-obj)<th)
       break; 
    end
    oldobj = obj;
end

end
